

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

public class Employee {

    public static void main(String[] args) {

        int idEdit,idRemove;

        String choise,idUpdate,idDelete;

        boolean isStop=false;

        Scanner sc=new Scanner(System.in);

        boolean flag;

        String digit = ".*\\w.*[a-zA-Z]";String digit1 = "\\d",digit2=".*[^0-9].*";

        ArrayList<StaffMember> staffMembers=new ArrayList<StaffMember>();

        StaffMember volunteer=new Volunteer(1,"Prem Leapheng","BMC");

        StaffMember salaryEmployee=new SalariedEmployee(2,"Heng Chantrea","BMC",543.50,50);

        StaffMember hourlyEmployee=new HourlyEmployee(3,"Dara kok","PP",80,7.5);

        Comparator<StaffMember> compareByName = (StaffMember sm1, StaffMember sm2) -> sm1.getName().compareTo( sm2.getName() );

        System.out.println("==============================================================================================");
        System.out.println("======================================= Display All Staff ====================================");
        System.out.println("==============================================================================================");

        staffMembers.add(volunteer);
        staffMembers.add(salaryEmployee);
        staffMembers.add(hourlyEmployee);

        do{

            Collections.sort(staffMembers, compareByName);
            for(StaffMember sm:staffMembers){
                System.out.println(sm);
                if(sm.pay()==0){
                    System.out.println("Thank!");
                }else{
                    System.out.println("Payment : "+sm.pay());
                }
                System.out.println("........................................");
                System.out.println();
            }

            System.out.println("------------------------------------------------------------------");
            System.out.println("------------------------Employee Management-----------------------");
            System.out.println("------------------------------------------------------------------");
            System.out.println("1- Add Employee");
            System.out.println("2- Edit Employee");
            System.out.println("3- Remove Employee");
            System.out.println("4- Exit program");
            System.out.println("--------------------------------------------------------------------");
            do {
                System.out.print("=> Choose the option (1-4) : ");
                choise=sc.next();
                flag=choise.matches(digit1);
                System.out.println();
                if (!flag){
                    System.out.println("You can choose only 1 until 4, Please choose again!");
                }
            } while (!flag);
            switch(choise){
                case "1":
                    String chooseAdd;
                    do{
                        Collections.sort(staffMembers, compareByName);
                        for(StaffMember sm:staffMembers){
                            System.out.println(sm);
                            if(sm.pay()==0){
                                System.out.println("Thank!");
                            }else{
                                System.out.println("Payment : "+sm.pay());
                            }
                            System.out.println("........................................");
                            System.out.println();
                        }

                        System.out.println("------------------------------------------------------------------");
                        System.out.println("----------------------------Add Employee--------------------------");
                        System.out.println("------------------------------------------------------------------");
                        System.out.println("1- Volunteer Employee");
                        System.out.println("2- Hourly Employee");
                        System.out.println("3- Salary Employee");
                        System.out.println("4- Back");
                        System.out.println("--------------------------------------------------------------------");
                        do {
                            System.out.print("=> Choose the option (1-4) : ");
                            chooseAdd=sc.next();
                            flag=chooseAdd.matches(digit1);
                            System.out.println();
                            if (!flag){
                                System.out.println("You can choose only 1 until 4, Please choose again!");
                            }
                        } while (!flag);
                        switch (chooseAdd){
                            case "1":
                                inputVolunteer(sc,staffMembers);
                                isStop=true;
                                break;
                            case "2":
                                inputHourlyEmployee(sc,staffMembers);
                                isStop=true;
                                break;
                            case "3":
                                inputSalaryEmployee(sc,staffMembers);
                                isStop=true;
                                break;
                            case "4":
                                isStop=false;
                                break;
                            default:
                                System.out.println("You can choose only 1 until 4, Please choose again!");
                                pressEnterKey();
                                break;
                        }

                    }while(isStop!=false);
                   break;
                case "2":
                    do {
                        System.out.print("=>Enter Employee ID to Update: ");
                        idUpdate=sc.next();
                        flag=idUpdate.matches(digit1);
                        if (!flag){
                            System.out.println("Input only number!");
                        }
                    } while (!flag);
                    idEdit=Integer.parseInt(idUpdate);

                    for(StaffMember sm:staffMembers){
                        if(sm.getId()==idEdit){
                            System.out.println(sm);
                            if(sm.pay()==0){
                                System.out.println("Thank!");
                            }else{
                                System.out.println("Payment : "+sm.pay());
                            }
                            System.out.println("-----------------------------------------------------");
                            if(sm instanceof Volunteer){
                                updateVolunteerEmployee(sc,sm);
                                isStop=true;
                            }else if(sm instanceof SalariedEmployee){
                                updateSalaryEmployee(sc,sm);
                                isStop=true;
                            }else if(sm instanceof HourlyEmployee){
                                updateHourlyEmployee(sc,sm);
                                isStop=true;
                            }
                        }
                    }
                    if(isStop==true){
                        System.out.println();
                        System.out.println("Update successfuly!");
                        System.out.println("-------------------------------------------------------");
                    }else{
                        System.out.println();
                        System.out.println("ID Not Found!");
                        System.out.println("-------------------------------------------------------");
                    }
                    break;
                case "3":
                    do {
                        System.out.print("=>Enter Employee ID to Delete : ");
                        idDelete=sc.next();
                        flag=idDelete.matches(digit1);
                        if (!flag){
                            System.out.println("Input only number!");
                        }
                    } while (!flag);
                    idRemove=Integer.parseInt(idDelete);
                    Iterator<StaffMember> iter = staffMembers.iterator();
                    while(iter.hasNext()){
                        StaffMember sm=iter.next();
                        if( sm.getId()==idRemove){
                            iter.remove();
                            isStop=true;
                        }
                    }
                    if(isStop==true){
                        System.out.println();
                        System.out.println("Delete successfuly!");
                        System.out.println("-------------------------------------------------------");
                    }else{
                        System.out.println();
                        System.out.println("ID Not Found!");
                        System.out.println("-------------------------------------------------------");
                    }
                    break;
                case "4":
                    System.out.println("(^-^) Good Bye! (^-^)");
                    System.exit(0);
                    break;
                default:
                    System.out.println("You can choose only 1 until 4, Please choose again!");
                    pressEnterKey();
                    break;
            }
        }while(true);

    }


//    ...............................................................................................
//    ................................. START PRESS ENTER KEY .......................................
//    ...............................................................................................
    public static void pressEnterKey(){
        System.out.println("Press enter to continue...");
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//    ...............................................................................................
//    ................................. END PRESS ENTER KEY .........................................
//    ...............................................................................................

//    ...............................................................................................
//    .................................. START INPUT VOLUNTEER EMPLOYEE .............................
//    ...............................................................................................
public static void inputVolunteer(Scanner input,ArrayList<StaffMember> staffMemberParameter){
    boolean flag;
    int id;
    String idConvert;
    String name,address;
    String digit = ".*\\w.*[a-zA-Z]";
    String digit1 = "\\d+";
    System.out.println(" ================= ADD Volunteer Employee =================");

    do {
        System.out.print("=> Enter Staff Member's ID   : ");
        idConvert=input.next();
        flag=idConvert.matches(digit1);
        if (!flag){
            System.out.println("Input only number!");
        }
    } while (!flag);
    id=Integer.parseInt(idConvert);
    input.nextLine();
    do {
        System.out.print("=> Enter Staff Member's Name : ");
        name=input.nextLine();
        flag=name.matches(digit);
        if (!flag){
            System.out.println("Input only string!");
        }
    } while (!flag);

    do {
        System.out.print("=> Enter Staff Member's Address : ");
        address=input.nextLine();
        flag=address.matches(digit);
        if (!flag){
            System.out.println("Input only string!");
        }
    } while (!flag);
    String subname1=name.substring(0,1).toUpperCase();
    String subname2=name.substring(1);

    staffMemberParameter.add(new Volunteer(id,subname1+subname2,address));
    System.out.println("..........................................................");
    System.out.println();
}
//    ...............................................................................................
//    .................................. END INPUT VOLUNTEER EMPLOYEE ...............................
//    ...............................................................................................

//    ...............................................................................................
//    .................................. START INPUT HOUR EMPLOYEE ..................................
//    ...............................................................................................
    public static void inputHourlyEmployee(Scanner input,ArrayList<StaffMember> staffMemberParameter){
        boolean flag;
        int id,hour;
        double rate=0;

        String idConvert;
        String name,address,hourConvert,rateConvert;
        String digit = ".*\\w.*[a-zA-Z]";
        String digit1 = "\\d+";
        System.out.println(" ================= ADD Hourly Employee =================");

        do {
            System.out.print("=> Enter Staff Member's ID     : ");
            idConvert=input.next();
            flag=idConvert.matches(digit1);
            if (!flag){
                System.out.println("Input only number!");
            }
        } while (!flag);

        input.nextLine();
        do {
            System.out.print("=> Enter Staff Member's Name : ");
            name=input.nextLine();
            flag=name.matches(digit);
            if (!flag){
                System.out.println("Input only string!");
            }
        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Address : ");
            address=input.nextLine();
            flag=address.matches(digit);
            if (!flag){
                System.out.println("Input only string!");
            }
        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Hour : ");
            hourConvert=input.nextLine();
            flag=hourConvert.matches(digit1);
            if (!flag){
                System.out.println("Input only string!");
            }
        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Rate : ");
            if(input.hasNextDouble()){
                rate=input.nextDouble();
                flag=true;
            }else{
                System.out.println("Input only number!");
                flag=false;
                input.next();
            }

        } while (!flag);
        String subname1=name.substring(0,1).toUpperCase();
        String subname2=name.substring(1);
        id=Integer.parseInt(idConvert);
        hour=Integer.parseInt(hourConvert);
        staffMemberParameter.add(new HourlyEmployee(id,subname1+subname2,address,hour,rate));
        System.out.println("..........................................................");
        System.out.println();
    }
//    ...............................................................................................
//    ................................... END INPUT HOUR EMPLOYEE ...................................
//    ...............................................................................................

//    ...............................................................................................
//    .................................. START INPUT SALARY EMPLOYEE ................................
//    ...............................................................................................
    public static void inputSalaryEmployee(Scanner input,ArrayList<StaffMember> staffMemberParameter){
        boolean flag;
        int id;
        double salary=0,bonus=0;

        String idConvert;
        String name,address;
        String digit = ".*\\w.*[a-zA-Z]";
        String digit1 = "\\d+";
        System.out.println(" ================= ADD Salary Employee =================");

        do {
            System.out.print("=> Enter Staff Member's ID     : ");
            idConvert=input.next();
            flag=idConvert.matches(digit1);
            if (!flag){
                System.out.println("Input only number!");
            }
        } while (!flag);

        input.nextLine();
        do {
            System.out.print("=> Enter Staff Member's Name : ");
            name=input.nextLine();
            flag=name.matches(digit);
            if (!flag){
                System.out.println("Input only string!");
            }
        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Address : ");
            address=input.nextLine();
            flag=address.matches(digit);
            if (!flag){
                System.out.println("Input only string!");
            }
        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Salary : ");
            if(input.hasNextDouble()){
                salary=input.nextDouble();
                flag=true;
            }else{
                System.out.println("Input only number!");
                flag=false;
                input.next();
            }

        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Bonus : ");
            if(input.hasNextDouble()){
                bonus=input.nextDouble();
                flag=true;
            }else{
                System.out.println("Input only number!");
                flag=false;
                input.next();
            }

        } while (!flag);
        String subname1=name.substring(0,1).toUpperCase();
        String subname2=name.substring(1);
        id=Integer.parseInt(idConvert);
        staffMemberParameter.add(new SalariedEmployee(id,subname1+subname2,address,salary,bonus));
        System.out.println("..........................................................");
        System.out.println();
    }
//    ...............................................................................................
//    .................................. END INPUT SALARY EMPLOYEE ..................................
//    ...............................................................................................

//    ...............................................................................................
//    .................................. START UPDATE VOLUNTEER EMPLOYEE ............................
//    ...............................................................................................
    public static void updateVolunteerEmployee(Scanner input,StaffMember volunteer){
        boolean flag;
        String name,address;
        String digit = ".*\\w.*[a-zA-Z]";
        String digit1 = "\\d+";
        System.out.println(" ================= Update Volunteer Employee =================");
        input.nextLine();
        do {
            System.out.print("=> Enter Staff Member's Name : ");
            name=input.nextLine();
            flag=name.matches(digit);
            if (!flag){
                System.out.println("Input only string!");
            }
        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Address : ");
            address=input.nextLine();
            flag=address.matches(digit);
            if (!flag){
                System.out.println("Input only string!");
            }
        } while (!flag);
        String subname1=name.substring(0,1).toUpperCase();
        String subname2=name.substring(1);
        volunteer.setName(subname1+subname2);
        volunteer.setAddress(address);
        System.out.println("..........................................................");
        System.out.println();
    }
//    ...............................................................................................
//    .................................. END INPUT VOLUNTEER EMPLOYEE ...............................
//    ...............................................................................................

//    ...............................................................................................
//    .................................. START UPDATE SALARY EMPLOYEE ...............................
//    ...............................................................................................
    public static void updateSalaryEmployee(Scanner input,StaffMember salaryEmployee ){
        boolean flag;
        int id;
        double salary=0,bonus=0;

        String name,address;
        String digit = ".*\\w.*[a-zA-Z]";
        String digit1 = "\\d+";
        System.out.println(" ================= Update Salary Employee =================");

        input.nextLine();
        do {
            System.out.print("=> Enter Staff Member's Name : ");
            name=input.nextLine();
            flag=name.matches(digit);
            if (!flag){
                System.out.println("Input only string!");
            }
        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Address : ");
            address=input.nextLine();
            flag=address.matches(digit);
            if (!flag){
                System.out.println("Input only string!");
            }
        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Salary : ");
            if(input.hasNextDouble()){
                salary=input.nextDouble();
                flag=true;
            }else{
                System.out.println("Input only number!");
                flag=false;
                input.next();
            }

        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Bonus : ");
            if(input.hasNextDouble()){
                bonus=input.nextDouble();
                flag=true;
            }else{
                System.out.println("Input only number!");
                flag=false;
                input.next();
            }

        } while (!flag);
        String subname1=name.substring(0,1).toUpperCase();
        String subname2=name.substring(1);
        salaryEmployee.setName(subname1+subname2);
        salaryEmployee.setAddress(address);
        ((SalariedEmployee) salaryEmployee).setSalary(salary);
        ((SalariedEmployee) salaryEmployee).setBonus(bonus);
        System.out.println("..........................................................");
        System.out.println();
    }
//    ...............................................................................................
//    .................................. END UPDATE SALARY EMPLOYEE .................................
//    ...............................................................................................

//    ...............................................................................................
//    .................................. START UPDATE HOUR EMPLOYEE .................................
//    ...............................................................................................
    public static void updateHourlyEmployee(Scanner input,StaffMember hourlyEmployee){
        boolean flag;
        int id,hour;
        double rate=0;

        String name,address,hourConvert;
        String digit = ".*\\w.*[a-zA-Z]";
        String digit1 = "\\d+";
        System.out.println(" ================= Update Hourly Employee =================");

        input.nextLine();
        do {
            System.out.print("=> Enter Staff Member's Name : ");
            name=input.nextLine();
            flag=name.matches(digit);
            if (!flag){
                System.out.println("Input only string!");
            }
        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Address : ");
            address=input.nextLine();
            flag=address.matches(digit);
            if (!flag){
                System.out.println("Input only string!");
            }
        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Hour : ");
            hourConvert=input.nextLine();
            flag=hourConvert.matches(digit1);
            if (!flag){
                System.out.println("Input only string!");
            }
        } while (!flag);

        do {
            System.out.print("=> Enter Staff Member's Rate : ");
            if(input.hasNextDouble()){
                rate=input.nextDouble();
                flag=true;
            }else{
                System.out.println("Input only number!");
                flag=false;
                input.next();
            }
        } while (!flag);

        hour=Integer.parseInt(hourConvert);
        String subname1=name.substring(0,1).toUpperCase();
        String subname2=name.substring(1);
        hourlyEmployee.setName(subname1+subname2);
        hourlyEmployee.setAddress(address);
        ((HourlyEmployee) hourlyEmployee).setHoursWorked(hour);
        ((HourlyEmployee) hourlyEmployee).setRate(rate);
        System.out.println("..........................................................");
        System.out.println();
    }
//    ...............................................................................................
//    ................................... END UPDATE HOUR EMPLOYEE ..................................
//    ...............................................................................................

}