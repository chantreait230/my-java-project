import java.util.Comparator;

public abstract class StaffMember {
    protected int id;
    protected String name;
    protected String address;

    public StaffMember(){}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "StaffMember{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public StaffMember(int id, String name, String address){
        this.id=id;
        this.name=name;
        this.address=address;
    }
    abstract double pay();

    Comparator<StaffMember> compareByName = new Comparator<StaffMember>() {
        @Override
        public int compare(StaffMember sm1, StaffMember sm2) {
            return sm1.getName().compareTo(sm2.getName());
        }
    };

}
